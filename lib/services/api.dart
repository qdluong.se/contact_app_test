import 'dart:convert';
import 'dart:io';

import 'package:contact_app_test/models/employee.dart';
import 'package:get/get.dart';

class ApiProvider extends GetConnect {
  static const host = 'http://testapp.mobilesoft.cz';

  Future<Response<List<Employee>>> getEmployees() => get<List<Employee>>(
        '$host/api/employees',
        decoder: (obj) =>
            obj.map<Employee>((e) => Employee.fromJson(e)).toList(),
      );

  Future<Response<Employee>> getEmployeeDetails(int id) => get<Employee>(
        '$host/api/employees/$id',
        decoder: (obj) => Employee.fromJson(obj),
      );

  Future<Response<Employee>> postEmployee(
    Map<String, dynamic> obj,
    File? image,
  ) {
    final form = FormData({'json': json.encode(obj)});
    if (image != null) {
      form.files.add(MapEntry(
        'photo',
        MultipartFile(
          image,
          filename: 'image.jpg',
          contentType: 'image/jpeg',
        ),
      ));
    }
    return post<Employee>(
      '$host/api/employees',
      form,
      contentType: 'multipart/form-data',
      decoder: (obj) => Employee.fromJson(obj),
    );
  }
}
