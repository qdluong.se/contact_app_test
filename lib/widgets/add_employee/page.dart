import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller.dart';

class AddEmployeePage extends StatelessWidget {
  const AddEmployeePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = AddEmployeeController();

    return Scaffold(
      appBar: AppBar(
        title: const Text('New employee'),
        centerTitle: true,
        actions: [
          Obx(
            () => IconButton(
              onPressed:
                  controller.loading.value ? null : controller.createEmployee,
              icon: const Icon(Icons.check),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: controller.formKey,
          child: Column(
            children: [
              Row(
                children: [
                  Flexible(
                    flex: 1,
                    child: Obx(
                      () => InkWell(
                        child: AspectRatio(
                          aspectRatio: 1.0,
                          child: controller.photo.value == null
                              ? Container(
                                  color: Colors.grey,
                                  alignment: Alignment.center,
                                  child: const Icon(Icons.camera),
                                )
                              : Image.file(controller.photo.value!),
                        ),
                        onTap: controller.selectImage,
                      ),
                    ),
                  ),
                  const SizedBox(width: 8.0),
                  Flexible(
                    flex: 3,
                    child: Column(
                      children: [
                        TextFormField(
                          controller: controller.nameController,
                          validator: controller.validateName,
                          decoration: const InputDecoration(hintText: 'Name'),
                        ),
                        const SizedBox(height: 8.0),
                        TextFormField(
                          controller: controller.addressController,
                          validator: controller.validateAddress,
                          minLines: 2,
                          maxLines: 2,
                          decoration:
                              const InputDecoration(hintText: 'Address'),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 8.0),
              TextFormField(
                controller: controller.departmentController,
                validator: controller.validateDepartment,
                decoration: const InputDecoration(hintText: 'Department'),
              ),
              const SizedBox(height: 8.0),
              TextFormField(
                  controller: controller.salaryController,
                  validator: controller.validateSalary,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(hintText: 'Salary')),
              const SizedBox(height: 8.0),
            ],
          ),
        ),
      ),
    );
  }
}
