import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../services/api.dart';
import '../employees/controller.dart';

class AddEmployeeController extends GetxController {
  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final addressController = TextEditingController();
  final departmentController = TextEditingController();
  final salaryController = TextEditingController();

  Rx<File?> photo = Rx(null);
  RxBool loading = RxBool(false);

  Future<void> createEmployee() async {
    if ((formKey.currentState?.validate() ?? false) && !loading.value) {
      loading.value = true;
      try {
        final result = await ApiProvider().postEmployee({
          'name': nameController.text,
          if (addressController.text.isNotEmpty)
            'address': addressController.text,
          if (departmentController.text.isNotEmpty)
            'department': departmentController.text,
          if (salaryController.text.isNotEmpty)
            'salary': int.parse(salaryController.text),
        }, photo.value);
        if (result.hasError || result.body == null) {
          Get.snackbar(
            'An error has occured',
            'Adding this employee failed.',
          );
        } else {
          Get.find<EmployeesController>()
              .database
              .employeeDao
              .addEmployee(result.body!);
          Get.back();
        }
      } catch (e) {
        Get.snackbar(
          'An error has occured',
          'Adding this employee failed.',
        );
      }
      loading.value = false;
    }
  }

  Future<void> selectImage() async {
    Get.bottomSheet(
      BottomSheet(
        onClosing: () {},
        builder: (context) => ListView(
          children: [
            ListTile(
              leading: const Icon(Icons.camera_alt),
              title: const Text('Use camera'),
              onTap: () {
                Get.back();
                ImagePicker()
                    .pickImage(source: ImageSource.camera)
                    .then((image) async {
                  photo.value = File(image!.path);
                });
              },
            ),
            ListTile(
              leading: const Icon(Icons.photo),
              title: const Text('Pick from gallery'),
              onTap: () {
                Get.back();
                ImagePicker()
                    .pickImage(source: ImageSource.gallery)
                    .then((image) async {
                  photo.value = File(image!.path);
                });
              },
            ),
          ],
        ),
      ),
    );
  }

  String? validateName(String? input) {
    if (input == null || input.trim().isEmpty) {
      return 'This field cannot be empty';
    }
    return null;
  }

  String? validateAddress(String? input) {
    // if (input == null || input.trim().isEmpty) {
    //   return 'This field cannot be empty';
    // }
    return null;
  }

  String? validateDepartment(String? input) {
    // if (input == null || input.trim().isEmpty) {
    //   return 'This field cannot be empty';
    // }
    return null;
  }

  String? validateSalary(String? input) {
    if (input != null &&
        input.isNotEmpty &&
        (!RegExp(r'^[0-9]+$').hasMatch(input) || int.parse(input) < 0)) {
      return 'Please enter a valid salary.';
    }
    return null;
  }
}
