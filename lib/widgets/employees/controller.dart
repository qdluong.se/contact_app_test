import 'package:get/get.dart';

import '../../models/employee.dart';
import '../../services/api.dart';
import '../../services/database.dart';

class EmployeesController extends GetxController {
  var employeesList = <Employee>[].obs;
  late final AppDatabase database;

  @override
  void onReady() async {
    super.onReady();
    await fetchLocalData();
    await fetchRemoteData();
  }

  Future<void> fetchLocalData() async {
    database =
        await $FloorAppDatabase.databaseBuilder('appdatabase.db').build();
    database.employeeDao
        .onEmployees()
        .listen((data) => employeesList.value = data);
  }

  Future<void> fetchRemoteData() async {
    final result = await ApiProvider().getEmployees();
    if (result.hasError || result.body == null) {
      Get.snackbar(
        'An error has occured',
        'We couldn\'t load the employee list',
      );
      result.printError();
    } else {
      database.employeeDao.updateEmployees(result.body!);
    }
  }
}
