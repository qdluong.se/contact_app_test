import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../add_employee/page.dart';
import '../cached_image.dart';
import '../employee_details/page.dart';
import 'controller.dart';

class EmployeesPage extends StatelessWidget {
  const EmployeesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(EmployeesController());

    return Scaffold(
      appBar: AppBar(
        title: const Text('Employees'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () => Get.to(() => const AddEmployeePage()),
          ),
        ],
      ),
      body: Obx(
        () => controller.employeesList.isEmpty
            ? const Center(child: CircularProgressIndicator())
            : ListView.separated(
                separatorBuilder: (context, index) => const Divider(),
                itemBuilder: (context, index) => ListTile(
                  title: Text(controller.employeesList[index].name),
                  subtitle:
                      Text(controller.employeesList[index].department ?? ''),
                  leading: SizedBox(
                    width: 100,
                    height: 100,
                    child: CachedImage(
                      url: controller.employeesList[index].photoUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                  onTap: () => Get.to(
                    () => EmployeeDetailsPage(
                      employee: controller.employeesList[index],
                    ),
                  ),
                ),
                itemCount: controller.employeesList.length,
                padding: const EdgeInsets.all(16.0),
              ),
      ),
    );
  }
}
