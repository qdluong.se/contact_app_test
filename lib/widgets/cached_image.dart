import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../services/api.dart';

class CachedImage extends StatelessWidget {
  final String? url;
  final double? width;
  final double? height;
  final BoxFit? fit;

  const CachedImage({
    Key? key,
    this.url,
    this.width,
    this.height,
    this.fit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url == null) return const Placeholder();

    return CachedNetworkImage(
      imageUrl: '${ApiProvider.host}$url',
      progressIndicatorBuilder: (context, url, progress) => Center(
          child: CircularProgressIndicator(
        value: progress.progress,
      )),
      errorWidget: (context, url, error) => const Placeholder(),
      width: width,
      height: height,
      fit: fit,
    );
  }
}
