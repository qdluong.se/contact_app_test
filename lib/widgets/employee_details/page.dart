import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../models/employee.dart';
import '../cached_image.dart';
import 'controller.dart';

class EmployeeDetailsPage extends StatelessWidget {
  final Employee employee;

  const EmployeeDetailsPage({
    Key? key,
    required this.employee,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(
      EmployeeDetailsController(employee),
      tag: employee.id.toString(),
    );

    return Scaffold(
      appBar: AppBar(
        title: Obx(() => Text(controller.employee.value.name)),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CachedImage(url: controller.employee.value.photoUrl),
            Row(
              children: [
                const SizedBox(width: 16.0),
                const Expanded(child: Text('Department')),
                Obx(() => Text(controller.employee.value.department ?? '-')),
                const SizedBox(width: 16.0),
              ],
            ),
            const SizedBox(height: 8.0),
            Row(
              children: [
                const SizedBox(width: 16.0),
                const Expanded(child: Text('Salary')),
                Obx(() =>
                    Text(controller.employee.value.salary?.toString() ?? '-')),
                const SizedBox(width: 16.0),
              ],
            ),
            const SizedBox(height: 8.0),
            Row(
              children: [
                const SizedBox(width: 16.0),
                const Expanded(child: Text('Address')),
                Obx(() => Text(controller.employee.value.address ?? '-')),
                const SizedBox(width: 16.0),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
