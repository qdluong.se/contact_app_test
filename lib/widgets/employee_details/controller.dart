import 'package:get/get.dart';

import '../../models/employee.dart';
import '../../services/api.dart';

class EmployeeDetailsController extends GetxController {
  EmployeeDetailsController(Employee employee) {
    this.employee = employee.obs;
  }

  late Rx<Employee> employee;

  @override
  void onReady() {
    super.onReady();
    fetchRemoteData();
  }

  void fetchRemoteData() async {
    final result = await ApiProvider().getEmployeeDetails(employee.value.id);
    if (result.hasError || result.body == null) {
      Get.snackbar(
        'An error has occured',
        'We couldn\'t load the data for this employee.',
      );
    } else {
      employee.value = result.body!;
    }
  }
}
