import 'package:floor/floor.dart';

@entity
class Employee {
  @primaryKey
  final int id;

  final String name;

  final String? address;

  final String? department;

  final int? salary;

  final String? photoUrl;

  const Employee(
    this.id,
    this.name,
    this.address,
    this.department,
    this.salary,
    this.photoUrl,
  );

  factory Employee.fromJson(Map<String, dynamic> json) {
    return Employee(
      json['id'],
      json['name'],
      json['address'],
      json['department'],
      json['salary'],
      json['photoUrl'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'address': address,
        'department': department,
        'salary': salary,
        'photoUrl': photoUrl,
      };
}
