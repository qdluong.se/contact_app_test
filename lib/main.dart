import 'package:contact_app_test/widgets/employees/page.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

void main() {
  runApp(const GetMaterialApp(home: EmployeesPage()));
}
