import 'package:contact_app_test/models/employee.dart';
import 'package:floor/floor.dart';

@dao
abstract class EmployeeDao {
  @Query('SELECT * FROM Employee')
  Stream<List<Employee>> onEmployees();

  @insert
  Future<void> addEmployee(Employee employee);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> updateEmployees(List<Employee> employees);
}
